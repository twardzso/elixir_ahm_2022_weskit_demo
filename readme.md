# Preparation

* Install conda environment and activate

```bash
conda env create -n weskit_demo -f environment.yaml
conda activate weskit_demo
```

# Basics

* First, start python and load required packages

```python
import requests, json, yaml, pprint, os
pp = pprint.PrettyPrinter(indent=2)

mywes = "https://weskit.bihealth.org"
```

* client functions

```python
verify=False

# get service info
def get_weskit_info(wes_url):
  info = requests.get("{}/ga4gh/wes/v1/service-info".format(wes_url), verify=verify)
  pp.pprint(info.json())

# get all runs
def get_weskit_runs(wes_url):
  runs = requests.get("{}/ga4gh/wes/v1/runs".format(wes_url), verify=verify)
  pp.pprint(runs.json())

# an extended version of all runs provided by weskit
def get_weskit_runs_ext(wes_url):
  runs_ext = requests.get("{}/weskit/v1/runs".format(wes_url), verify=verify)
  pp.pprint(runs_ext.json())

def get_weskit_run_status(wes_url, run_id):
  run_status = requests.get("{}/ga4gh/wes/v1/runs/{}/status".format(wes_url, run_id), verify=verify)
  pp.pprint(run_status.json())

# info
def get_weskit_run_info(wes_url, run_id):
  run_info = requests.get("{}/ga4gh/wes/v1/runs/{}".format(wes_url, run_id), verify=verify)
  pp.pprint(run_info.json())

# stdout
def get_weskit_run_stdout(wes_url, run_id):
  run_out = requests.get("{}/weskit/v1/runs/{}/stdout".format(wes_url, run_id), verify=verify)
  pp.pprint(run_out.json())

# stderr
def get_weskit_run_stderr(wes_url, run_id):
  run_err = requests.get("{}/weskit/v1/runs/{}/stderr".format(wes_url, run_id), verify=verify)
  pp.pprint(run_err.json())

# send workflow
def send_snakemake_workflow(wes_url, config, files):
  workflow_params = json.dumps(config)
  ## create data object for request
  data = {
    "workflow_params": workflow_params,
    "workflow_type": "SMK",
    "workflow_type_version": "6.10.0",
    "workflow_url": "Snakefile",
    "workflow_engine_parameters": "{}"
  }
  response = requests.post("{}/ga4gh/wes/v1/runs".format(wes_url), data=data, files=files, verify=False)
  pp.pprint(response.json())
  return response

def send_nextflow_workflow(wes_url, config, files):
  workflow_params = json.dumps(config)
  ## create data object for request
  data = {
    "workflow_params": workflow_params,
    "workflow_type": "NFL",
    "workflow_type_version": "21.04.0",
    "workflow_url": "workflow.nf",
    "workflow_engine_parameters": "{}"
  }
  response = requests.post("{}/ga4gh/wes/v1/runs".format(wes_url), data=data, files=files, verify=False)
  pp.pprint(response.json())
  return response
```

# Check endpoint

* Lets check the weskit endpoint:

```python
get_weskit_info(mywes)
get_weskit_runs(mywes)
get_weskit_runs_ext(mywes)
```

# Snakemake

```python
## attach workflow files
files = [
  ("workflow_attachment", ("Snakefile", open("workflows/snakemake_wf1/Snakefile", "rb")))
]

config =  {"text": "hello world", "outpath": "twardzso/testout_bih_3"}

run_snakemake_wf1 = send_snakemake_workflow(wes_url=mywes, config=config, files=files)

get_weskit_run_info(mywes, run_snakemake_wf1.json()["run_id"])
get_weskit_run_stdout(mywes, run_snakemake_wf1.json()["run_id"])
get_weskit_run_stderr(mywes, run_snakemake_wf1.json()["run_id"])
```

## Using Krini:

* Workflow URL: Snakefile
* Workflow params: {"text": "hello world", "outpath": "twardzso/testout_bih_3"}
* Workflow attachments: workflows/snakemake_wf1/Snakefile

# Nextflow

```python
files = [
  ("workflow_attachment", ("workflow.nf", open("workflows/nextflow_1/workflow.nf", "rb")))
]

config =  {"text": "hello world"}

run_nfl_1 = send_nextflow_workflow(wes_url=mywes, config=config, files=files)

get_weskit_run_info(mywes, run_nfl_1.json()["run_id"])
get_weskit_run_stdout(mywes, run_nfl_1.json()["run_id"])
get_weskit_run_stderr(mywes, run_nfl_1.json()["run_id"])
```
## Using Krini:

* Workflow URL: workflow.nf
* Workflow params: {"text": "hello world"}
* Workflow attachments: workflows/nextflow_1/workflow.nf


